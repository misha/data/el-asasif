.. This file should at least contain the root `toctree` directive.

Documentation du projet El-Asasif
=================================

.. toctree::
  :maxdepth: 2
  :caption: Table des matières:

  modules

Index
=====

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
