# -*- coding: utf-8 -*-
from setuptools import setup, find_packages


setup(
    name='asasif',
    version='1.0.0',
    url='https://gitlab.huma-num.fr/misha/data/el-asasif.git',
    author='Régis Witz',
    author_email='regis.witz@cnrs.fr',
    description='El-Assasif data Nakala uploader.',
    packages=find_packages(),
)
