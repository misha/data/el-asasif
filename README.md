[![CI status](https://gitlab.huma-num.fr/misha/data/el-asasif/badges/main/pipeline.svg)](../../pipelines)
[![Coverage report](https://gitlab.huma-num.fr/misha/data/el-asasif/badges/main/coverage.svg)](../../commits/master)

Ce dépôt contient le programme d'upload sur Nakala des photographies du projet El-Asasif, porté par Cassandre Hartenstein et Frédéric Colin.

Le code est documenté [à cette adresse](https://misha.gitpages.huma-num.fr/data/el-asasif).

La manière dont cet upload est réalisé est documenté [sur cette page](../../wikis/nakala_upload).

🚧
Le programme se lance de cette manière :
```console
python3 -m src.nakala.upload --files-dir resources/Croquis/ xml/croquis.xml
```
