# -*- coding: utf-8 -*-


OBJECTS_COLUMNS_VS_FIELDS = {
        'Objet H-ID': 'hid',
        'title': 'title',
        'us.title': 'us',
        'us.hid': None,
        'numero': 'numero',
    }


def write_object(writer, o):
    """
    Writes a row of the .csv output file.
    Each line contains an entry of the input .csv which image was uploaded.
    @params:
        writer    - Required : writer to output file (we always output csv)
        o         - Required : Object info
    """
    hid = getattr(o, OBJECTS_COLUMNS_VS_FIELDS['Objet H-ID'])
    numero = getattr(o, OBJECTS_COLUMNS_VS_FIELDS['numero'])
    us = getattr(o, OBJECTS_COLUMNS_VS_FIELDS['us.title'])
    writer.writerow([hid, '', '', us, numero])


def write_objects_header(writer):
    writer.writerow(OBJECTS_COLUMNS_VS_FIELDS.keys())


PHOTOS_FIELDS_VS_COLUMNS = {
        'reference': 'Renommée',
        'objets': 'Objets H-ID',
        'path': 'Fichier local',
        'device': 'Appareil',
        'name_on_device': 'Nom sur l\'appareil',
        'auteur_photo': 'Auteur photo',
        'date_photo': 'Date photo',
        'auteur_saisie': 'Auteur saisie',
        'date_saisie': 'Date saisie',
        'description': 'Description',
        'us': 'US',
        'terrain': 'Terrain?',
        'in_situ': 'In situ',
        'definition': 'Définition',
    }


def write_photo(writer, photo):
    """
    Writes a row of the .csv output file.
    Each line contains an entry of the input .csv which image was uploaded.
    @params:
        writer    - Required : writer to output file (we always output csv)
        photo     - Required : Photo info
    """
    row = []
    for field in PHOTOS_FIELDS_VS_COLUMNS.keys():
        if field == 'objets':
            objects = getattr(photo, field, [])
            row.append('|'.join([str(o.hid) for o in objects]))
            continue
        row.append(getattr(photo, field, ''))
    writer.writerow(row)


def write_photos_header(writer):
    writer.writerow(PHOTOS_FIELDS_VS_COLUMNS.values())
