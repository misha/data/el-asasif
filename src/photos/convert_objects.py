# -*- coding: utf-8 -*-
import argparse
import csv
from ..nakala.upload import get_columns
from ..nakala.cli import progress


def positive_int(value):
    try:
        ivalue = int(value)
        if ivalue > 0:
            return ivalue
        raise argparse.ArgumentTypeError(
                "Invalid positive integer value: '%s'" % value)
    except ValueError:
        raise argparse.ArgumentTypeError(
                "Invalid positive integer value: '%s'" % value)


def create_parser():
    """
    Creates a CLI argument parser for this app.
    """
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter
            )
    parser.add_argument(
            'photos',
            help="Photos file path (CSV file)")
    parser.add_argument(
            'objets',
            help="Objects file path (CSV file)")
    parser.add_argument(
            '-o', '--output', default='photos.csv',
            help="Updated photos output file path (CSV file)")
    parser.add_argument(
            '-s', '--separator', default=';',
            help="CSV file fields separator")
    parser.add_argument(
            '--last-hid', type=positive_int,
            help="First H-ID tu use for missing objects, if any")
    parser.add_argument(
            '--missing-objects-csv', default='missing_objects.csv',
            help="Missing objects output, if any (CSV file)")
    return parser


def read_csv(path, delimiter, columns, callback):
    with open(path, newline='', encoding='utf-8-sig') as f:
        reader = csv.reader(f, delimiter=delimiter, quotechar='"')
        # read first row
        columns = get_columns(reader, columns)
        # read last rows (ie. every row excluding the first)
        for row in progress(list(reader)):
            callback(columns, row)


class Object(object):

    def __init__(self, hid, title, us, numero):
        self.hid = hid
        self.title = title
        self.us = us
        self.numero = numero

    def __repr__(self):
        return f"{self.hid}:{self.title}"

    @staticmethod
    def find(objects, fieldname, value):
        for o in objects:
            if (getattr(o, fieldname) == value):
                return o
        return None


DEVICES = {
    'Nikon D500': '1_D500',
    }


class Photo(object):

    def __init__(self, row, columns, objets=None):
        self.reference = row[columns['Renommée']]
        self.device = row[columns['Appareil']]
        self.name_on_device = row[columns['Nom sur l\'appareil']]
        self.auteur_photo = row[columns['Auteur photo']]
        self.date_photo = row[columns['Date photo']]
        self.auteur_saisie = row[columns['Auteur saisie']]
        self.date_saisie = row[columns['Date saisie']]
        self.description = row[columns['Description']]
        self.us = row[columns['US']]
        self.terrain = row[columns['Terrain?']]
        self.in_situ = row[columns['In situ']]
        self.definition = row[columns['Définition']]
        device_dir = DEVICES[self.device]
        self.path = 'http://localhost:8000/Photos/2021/'+device_dir+'/Mini_Photos/Mini_'+self.reference+'.jpeg'  # noqa: E501
        self.objets = objets or []

    def __repr__(self):
        return f"{self.reference}:{self.device}:{self.objets}"


if __name__ == '__main__':

    args = create_parser().parse_args()

    objects = []

    def on_object(columns, row):
        hid = int(row[columns['hid']])
        title = row[columns['title']]
        us = row[columns['us.title']]
        numero = row[columns['numero']]
        objects.append(Object(hid, title, us, numero))
    read_csv(args.objets, args.separator, [
            'hid', 'title', 'us.hid', 'us.title', 'numero',
            ], on_object)
    print(str(len(objects))+" objects loaded.")

    photos = []
    missing = {}
    count = [0, 0]

    def on_photo(columns, row):
        count[0] += 1
        obj_titles = row[columns['Référence objet']]
        obj_titles = obj_titles.split('|')
        objs = []

        for title in obj_titles:
            if not title:  # this photo shows no object
                continue   # so we've nothing to do
            o = Object.find(objects, 'title', title)
            if not o:
                count[1] += 1
                o = missing.get(title, None)
                if not o:
                    args.last_hid += 1
                    hid = args.last_hid
                    o = Object(hid, title, '', title)
                    missing[title] = o
            objs.append(o)
        photos.append(Photo(row, columns, objs))
    read_csv(args.photos, args.separator, [
            'Nom sur l\'appareil', 'Renommée', 'Auteur photo', 'Date photo',
            'Description', 'US', 'Terrain?',
            'Référence objet', 'In situ', 'Définition',
            'Auteur saisie', 'Date saisie', 'Appareil',
            ], on_photo)
    print(str(len(photos))+" photos found.")

    with open(args.output, 'w', newline='') as f:
        writer = csv.writer(f, delimiter=args.separator, quotechar='"')
        from .csv import write_photo, write_photos_header
        write_photos_header(writer)
        for photo in photos:
            write_photo(writer, photo)

    print((str(count[1])+"("+str(len(missing))+" unique) objects missing"
          "on "+str(count[0])+" lines."))
    print(missing)

    if len(missing) > 0:
        with open(args.missing_objects_csv, 'w', newline='') as f:
            writer = csv.writer(f, delimiter=args.separator, quotechar='"')
            from .csv import write_object, write_objects_header
            write_objects_header(writer)
            for key, o in missing.items():
                write_object(writer, o)
