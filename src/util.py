# -*- coding: utf-8 -*-
from os.path import join, splitext
from os import walk
from glob import glob


def list_files(path, extensions=None):
    """
    List files contained in a folder.

    You can filter the "file type(s)" you want using extensions parameter.
    If no extensions are provided, list_file will return everything.

    Really, this methods list _paths_ in a _path_, so behaviour is not
    guaranteed if you give it path to a file instead of to a folder.

    @params
        path - Required        : Folder path
        extensions - Optional  : tuple with authorized extension(s)
    """
    files = [y for x in walk(path) for y in glob(join(x[0], '*'))]
    if extensions:
        extensions = tuple((e.lower() for e in extensions))
        files = [f for f in files if f.lower().endswith(extensions)]
    return files


def get_prefix(reference, separator='_'):
    """
    Extract prefix from an input string representing a reference.

    Prefix ends at the last occurence of separator in input string.

    Examples of reference <> prefix:
    - CROQ_00109 <> CROQ_
    - AS_2022_00042 <> AS_2022_
    - WTF_01_WAT_TOTO <> WTF_01_WAT_

    @params
        reference - Required : input string
        separator - Optional : separator string
    """
    return separator.join(reference.split(separator)[0:-1])+separator


def path_vs_reference(path, reference):
    """
    Check if a file path belongs to a record, using its reference.

    This is assessed if the filename begins with the reference prefix,
    and if the number after the prefix is the same than in the filename.

    Really, this method is aimed at files like /dir/subdir/PREFIX_00042.EXT
    and at references which are like PREFIX_042 ; you _can_ have different
    length for numbers, but behaviour is not guaranted if you have complicated
    prefixes (@see get_prefix), numbers in the middle of the filename, prefix
    in the subdirectory, or whatever.

    @params
        path      - Required  : File path
        reference - Required  : Record reference (see .hml module)
    """
    if not reference:
        return False
    prefix = get_prefix(reference)
    # try to find prefix in path
    parts = splitext(path)
    index = parts[0].rfind(prefix)
    if index < 0:  # prefix not found
        return False
    # compute path ordinal number
    try:
        target = int(parts[0][index + len(prefix):])  # path ordinal
        # compute reference ordinal number
        index = reference.rfind(prefix) + len(prefix)
        candidate = int(reference[index:])  # reference ordinal
    except ValueError:  # try to compare non-ints
        return parts[0] == reference
    # if ordinals are the same, path matches record reference
    if target == candidate:
        return True
    return False
