# -*- coding: utf-8 -*-
from lxml import etree
from copy import copy
from .tables import Record, Person


MODELS = {
        'person': Person,
        }


def parse_tree(path):
    tree = None
    with open(path, 'r', encoding='utf-8') as fd:
        tree = etree.parse(fd)
    return tree


def parse(args, target_class, records=None):
    target = target_class() if target_class else Record()
    records = records or {}
    for path in args.input:
        tree = parse_tree(path)
        events = ('start', 'end')
        context = etree.iterwalk(tree.getroot(), events=events)
        for action, e in context:
            o = getattr(target, action)(e)
            if o:
                records[int(o.hid)] = copy(o)  # shallow copy
                o.reset()
    return records


'''
if __name__ == '__main__':
    args = create_parser().parse_args()
    target = MODELS[args.model]
    parse(args, target)
'''
