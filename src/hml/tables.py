# -*- coding: utf-8 -*-
from lxml.etree import QName


class Record(object):

    HML_TAGS = {
        # we use a dict so Heurist fields name do not
        # conflict with python objects default attrs
        'detail': None,  # define this in subclasses
        'id': 'hid',  # record identifier
        'type': 'type',  # record type identifier
        'added': 'date_added',
        'modified': 'date_modified',
        }
    FIELDS = list(HML_TAGS.values())
    TAG = 'record'
    DETAILS = {}

    def start(self, e):
        e.tag = QName(e).localname  # ~costly operation
        if e.tag in self.HML_TAGS.keys():
            attr, repeatable = self.getattr(e.tag)
            if attr == 'type':
                value = int(e.get('id'))  # DataType id
            elif attr == 'hid':
                p = e.getparent()
                if p.tag == 'record':
                    value = int(e.text)
                else:  # parent is probably <file>, and we dont want file id
                    return
            else:
                value = e.text

            if not attr and e.tag == 'detail':
                field_id = int(e.get('id'))
                attr, repeatable = self.getattr(field_id, attrs=self.DETAILS)
            self.setattr(attr, value, repeatable)
        return None

    def getattr(self, tag, default=None, attrs=None):
        attrs = attrs or self.HML_TAGS
        attr = attrs.get(tag, default)
        if type(attr) is tuple:
            return attr
        return attr, False

    def getattrs(self):
        res = []
        for attr in list(self.HML_TAGS.values()) + list(self.DETAILS.values()):
            if not attr:
                continue
            if type(attr) is tuple:
                attr = attr[0]
            res.append(attr)
        return res

    def setattr(self, attr, value, repeatable):
        if attr:
            if repeatable:
                lst = getattr(self, attr, [])
                lst.append(value)
                value = lst
            setattr(self, attr, value)

    def end(self, e):
        if e.tag == self.TAG and self.type == self.DATATYPE_ID:
            return self
        return None

    def reset(self):
        for attr in self.getattrs():
            if hasattr(self, attr):
                delattr(self, attr)

    def __repr__(self):
        return '%s:%s:%s' % (
                self.hid, self.type, self.__class__.__name__)


class Relationship(Record):
    DATATYPE_ID = 1
    DETAILS = {
        7: 'src',
        5: 'dst',
        6: 'label',
        }

    def __repr__(self):
        return '%s: %s --[%s]--> %s' % (
                self.hid, self.src, self.label, self.dst)


class Person(Record):
    DATATYPE_ID = 10
    DETAILS = {
        1: 'last_name',
        18: 'first_name',
        132: 'alternate_name',
        }

    def __repr__(self):
        res = ''
        s = getattr(self, 'last_name', None)
        if s:
            res += ("%s" % s)
        s = getattr(self, 'first_name', None)
        if s:
            res += (", %s" % s)
        s = getattr(self, 'alternate_name', None)
        if s:
            res += (" [%s]" % s)
        return '%s:%s %s' % (self.hid, self.__class__.__name__, res)


class Croquis(Record):
    DATATYPE_ID = 104
    DETAILS = {
        1180: 'reference',
        1102: ('creators', True),
        1151: ('contributors', True),
        1178: 'description',
        }

    def __repr__(self):
        res = ''
        s = getattr(self, 'reference', None)
        if s:
            res += ("%s" % s)
        s = getattr(self, 'creators', None)
        if s:
            res += (" by %s" % ', '.join(s))
        return '%s:%s %s' % (self.hid, self.__class__.__name__, res)
