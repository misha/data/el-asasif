# -*- coding: utf-8 -*-
import csv


class Reader(object):
    """
    Lecteur de fichiers CSV

    Typiquement, cette classe s'utilise comme ceci :

    .. highlight:: python
    .. code-block:: python

        reader = Reader()
        with open('mon/fichier.csv', newline='', encoding='utf-8-sig') as fd:
            reader.read(fd)

    Il suffira ensuite d'accéder aux données, soit directement avec le champ
    ``reader.rows``, soit en utilisant la méthode ``reader.get_cell``.

    Il est aussi possible de fournir une fonction ``callback`` dès la
    construction de l'instance.
    Dans ce cas, la classe s'utilise comme cela :

    .. highlight:: python
    .. code-block:: python

        def on_object(header, row):
            value = Reader.get('column_label', header, row)
            ...  # faites ce que vous voulez avec les données

        reader = Reader(callback=on_object)
        with open('mon/fichier.csv', newline='', encoding='utf-8-sig') as fd:
            reader.read(fd)

    Cette seconde alternative peut éviter de charger toutes les données
    en mémoire, car ``reader.rows`` n'est pas valué.
    """

    def __init__(self, callback=None, delimiter=',', quotechar='"'):
        self.delimiter = delimiter
        self.quotechar = quotechar
        self.header = []
        if callback:
            self.callback = callback
        else:
            self.rows = []

    def read(self, fd):
        """
        Lit un fichier CSV.

        Parameters
        ----------
        fd :
            descripteur de fichier
        """
        reader = csv.reader(
                fd,
                delimiter=self.delimiter,
                quotechar=self.quotechar)
        self.header = next(reader, None)
        if hasattr(self, 'callback'):
            return self._read_with_callback(reader)
        return self._read_rows(reader)

    def _read_with_callback(self, reader):
        for row in reader:
            self.callback(self.header, row)

    def _read_rows(self, reader):
        for row in reader:
            self.rows.append(row)

    def get_cell(self, row_index, column_name):
        """
        Renvoie le contenu d'une cellule d'un fichier CSV.
        Le fichier doit préalablement avoir été lu grâce à la méthode ``read``,
        et l'objet ``Reader`` créé sans utiliser de ``callback``.

        Parameters
        ----------
        row_index : str
            Indice de ligne (commençant à 0 ; en d'autre termes, l'indice 0 est
            celui de la première ligne d'un fichier CSV *après* l'en-tête)
        column_name : str
            Libellé de colonne
        """
        column_index = self.header.index(column_name)
        return self.rows[row_index][column_index]

    @staticmethod
    def get(column_name, header, row):
        """
        Renvoie le contenu d'une cellule d'une ligne donnée d'un fichier CSV.

        Typiquement, le paramètre ``row`` est le même que celui de la fonction
        ``callback``. Ce paramètre ``row`` peut aussi être obtenu en itérant
        sur une instance de ``csv.reader``, dont cette classe est un wrapper.

        Parameters
        ----------
        column_name : str
            Libellé de colonne
        header : list
            Liste des libellés de colonnes (*ie.* en-tête original du fichier)
        row : list
            Contenu de ligne
        """
        column_index = header.index(column_name)
        return row[column_index]
