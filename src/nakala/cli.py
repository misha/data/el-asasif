# -*- coding: utf-8 -*-
import argparse


def create_parser():
    """
    Creates a CLI argument parser for this app.
    """
    parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter
            )
    parser.add_argument(
            'input', nargs='+',
            help="Input file path (CSV file)")
    parser.add_argument(
            '--files-dir', default='images',
            help="Folder containing files to upload")
    parser.add_argument(
            '--items',
            help="""Items file path (CSV file)
* If present, files in --files-dir will be grouped in items
* If missing, one Nakala item will be created for each file in --files-dir
""")
    parser.add_argument(
            '-s', '--separator', default=';',
            help="input file fields separator")
    parser.add_argument(
            '-k', '--key', default='key',
            help="input file column name for unique key")
    parser.add_argument(
            '-a', '--creators', default='auteurs',
            help="input file column name for creators list")
    parser.add_argument(
            '-p', '--path', default='images_files',
            help="input file column name for files path")
    parser.add_argument(
            '-t', '--title', default='catalogue_titre',
            help="input file column name for file title")
    parser.add_argument(
            '-d', '--description', default='titre_normalise',
            help="input file column name for file comment / description")
    parser.add_argument(
            '-c', '--created', default='catalogue_edition_annee',
            help="input file column name for file creation date")
    parser.add_argument(
            '-x', '--issued', default='create_date',
            help="input file column name for file creation date")
    parser.add_argument(
            '-o', '--output', default='output.csv',
            help="output filename")
    parser.add_argument(
            '-l', '--log-file', default='output.log',
            help="Verbose mode.")
    parser.add_argument(
            '-v', '--verbose', action='store_true',
            help="Verbose mode.")
    parser.add_argument(
            '--cleanup', action='store_true',
            help="If true, delete everything just after its creation.")
    parser.add_argument(
            '--dry-run', action='store_true',
            help="If true, simulate operations, without uploading to Nakala.")
    return parser


def progress(
        iterable, length=70, decimals=1,
        prefix="", suffix="", full='█', empty='-', end="\r"
        ):
    """
    Use this method in a loop to display CLI output progress bar.
    @params:
        iterable - Required  : iterable to iterate on
        prefix   - Optional  : prefix string
        suffix   - Optional  : suffix string
        decimals - Optional  : number of decimals in completion percentage
        length   - Optional  : length of progress bar, in number of chars
        full     - Optional  : character for part of bar which filled
        empty    - Optional  : character for part of bar which is not filled
        end      - Optional  : end character (e.g. "\r", "\r\n")
    """
    total = len(iterable)  # iterations count

    def print_progress_bar(step):
        percent = ('{0:.%sf}' % decimals).format(100 * (step / float(total)))
        filled = int(length * step // total)  # filled length
        bar = full * filled + empty * (length - filled)
        print('\r%s [%s] %s%% %s' % (prefix, bar, percent, suffix), end=end)

    print_progress_bar(0)  # init
    # update progress bar
    for i, item in enumerate(iterable):
        yield item
        print_progress_bar(i + 1)
    print()  # print newline when completed
