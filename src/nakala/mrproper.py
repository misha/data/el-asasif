import cli
import settings
import requests
from os.path import join
from json import dumps

if __name__ == '__main__':
    headers = {
        'X-API-KEY': settings.API_KEY,
        'accept': 'application/json',
        'Content-Type': 'application/json'
        }
    data = {
        'page': 1,
        'limit': 200,
        'status': ['published'],
        'orders': ['creDate,desc']
        }
    baseurl = join(settings.API_URL, 'users', 'datas', 'owned')
    response = requests.post(baseurl, headers=headers, data=dumps(data))
    for data in cli.progress(response.json()['data']):
        id_ = data['identifier']
        for f in data['files']:
            hash_ = f['sha1']
            url = join(settings.API_URL, 'datas', id_, 'files', hash_)
            requests.delete(url, headers=headers)
        url = join(settings.API_URL, 'datas', id_)
        requests.delete(url, headers=headers)

        from time import sleep
        sleep(0.1)
