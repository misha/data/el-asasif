# -*- coding: utf-8 -*-
from .api import Collection, Data
from .cli import create_parser, progress
from .settings import REPO_URL, API_URL, COLLECTIONS
import logging
import csv
from os.path import isfile, join
from sys import exit


def get_columns(reader, names):
    columns = {}
    for first_row in reader:
        for column_name in names:
            try:
                columns[column_name] = first_row.index(column_name)
            except ValueError:
                columns[column_name] = None
        break
    return columns


def find_file(key, path):
    if isfile(path):
        return path
    if isfile(path.lower()):
        # files were originally hosted on a win$ OS, thus case insensitivity
        return path.lower()
    logging.error("❌;CREATE;Data;%9s;;;;%s;Does Not Exist" % (key, path))
    return None


def is_already_uploaded(key, path, done, rows):
    if path in done:
        other = done[path]
        handle = rows.get(other, {}).get('handle', None) or '~'
        info = (key, handle, path, other)
        logging.warning("⚠️;CREATE;Data;%9s;%s;;;%s;Already Done (%s)" % info)
        return other
    return None


def get_authors(authors_str, authors):
    if not authors_str:
        return []
    keys = authors_str.split('|')
    return [authors[int(a)] for a in keys]


def check_orphaned_files(images_dir, done):
    from os import walk
    from glob import glob
    # files were originally hosted on a win$ OS, thus case insensitivity
    left = [y.lower() for x in walk(images_dir) for y in glob(join(x[0], '*'))]
    for path in done:
        if path in left:
            left.remove(path)
        elif path.lower() in left:
            left.remove(path.lower())
    for path in left:
        logging.warning("⚠️;;;;;;;\"%s\";Orphan" % path)


def create_data(row, columns, args, creators, settings, paths=None):
    publisher = settings.get('publisher', None)
    rights = settings.get('rights', None)
    contributor = settings.get('creator', None)
    created = row[columns[args.created]] or None
    issued = row[columns[args.issued]].replace('/', '-')
    title = row[columns[args.title]]
    if not title:
        # value "" for http://nakala.fr/terms#title is unauthorized
        title = "Aucun titre"  # TODO i18n
    description = row[columns[args.description]]
    if not description:
        # value "" for http://purl.org/dc/terms#description is unauthorized
        description = "Pas de description"  # TODO i18n
    # Data uploaded to Nakala are private until you specify differently in
    # the settings. If you upload more than ~2Go of private data it will break
    public = settings.get('public', False)
    # Note: I don't really want to provide a default for license ; if you call
    # this script with no second thought regarding license it _should_ break.
    # Even if you leave Etalab licence in settings.py and you didn't want that,
    # you _should_ be safe if you left public as False.
    # If you didn't, too bad for you.
    license = settings['license']
    paths = paths or []
    return Data(
            public=public,
            rights=rights,
            license=license,
            collection=collection.id,
            name=title,
            description=description,
            contributor=contributor,
            creators=creators,
            created=created,
            issued=issued,
            publisher=publisher,
            paths=paths,
            )


def write_csv(writer, upload, baseurl, api_url, delimiter=','):
    """
    Writes a row of the .csv output file.
    Each line contains an entry of the input .csv which image was uploaded.
    @params:
        writer    - Required : writer to output file (we always output csv)
        upload    - Required : data upload info
        baseurl   - Required : Nakala repository base url
        api_url   - Required : Nakala API base url
        delimiter - Optional : field separator
    """
    key = upload['key']
    handle = upload['handle'] or '~'
    hash_ = upload['hash']
    web = join(baseurl, handle)
    raw = join(api_url, 'data', handle, hash_)
    writer.writerow([key, handle, hash_, web, raw])


def API_CALL(item, action, args, key=''):
    print(action.upper(), item, args, key)
    method = getattr(item, action)
    a, t = (action.upper(), type(item).__name__)
    if not args.dry_run:
        try:
            status = '✅' if method() else '❌'
            if args.verbose:
                logging.info("%s;%s;%s;%9s;%s;-", status, a, t, key, item)
        except Exception as ex:
            print(ex)
            logging.error("❌;%s;%s;%9s;%s;%s", a, t, key, item, ex)
    else:
        if args.verbose:
            logging.info("✅;%s;%s;%9s;%s;-", a, t, key, item)


if __name__ == '__main__':

    args = create_parser().parse_args()
    logging.basicConfig(
            filename=args.log_file,
            filemode='w',  # each run starts anew
            level=logging.INFO,
            format='%(asctime)s;%(levelname)-7s;%(message)s',
            )
    logging.info("status;action;type;key;handle;creator;created;path;message")
    if args.dry_run and args.verbose:
        print("⚠️  Simulation mode")
        logging.warning("⚠️;;;;;;;;Simulation mode")

    from ..hml.read import parse
    from ..hml.tables import Person, Croquis
    # as each H-ID is unique, we can put all records in the same map
    records = parse(args, Person)
    records = parse(args, Croquis, records)
    for hid, record in sorted(records.items()):
        print(record)

    SETTINGS = COLLECTIONS[0]
    collection = Collection(
            name=SETTINGS['name'],
            rights=SETTINGS['rights'],
            public=SETTINGS.get('public', False),
            )
    collection.id = '10.34847/nkl.637f68o6'  # TODO

    if collection.id:
        API_CALL(collection, 'retrieve', args)
    else:
        API_CALL(collection, 'create', args)
    if not args.dry_run and not collection.id:
        print("No --dry-run but no collection ID: abort!")
        exit()

    # This code has two responsibilities:
    # - populate items, a dictionnary of H-ID vs list of file paths
    #   each (hid,list) will become one Nakala item ;
    #   each path in the list will be a file of the item
    # - populate orphans, a list of file path belonging to no record
    #
    # There are two cases, depending if --items cli parameter is provided
    # - if it is, the csv is simply parsed and put into items dictionnary
    # - if not, items is created from records dictionnary ;
    #   each record H-ID becomes a key of items, its value being a list of
    #   only one element, this element being the file path corresponding to
    #   the record reference (eg. dir/subdir/PX_0042.JPG matches ref PX_042)
    items = {}  # H-ID vs file paths
    orphans = []  # files with no record
    if args.items:
        pass  # TODO: groups files into logical groups (ie. Nakala items)
    else:
        # No items CSV file has been provided, so create one item for each file
        # To achieve this, we shall list all files in args.files_dir
        from ..util import list_files, path_vs_reference
        files = list_files(args.files_dir, ('.jpg', ))

        for path in files:
            found = None
            for hid, record in sorted(records.items()):
                reference = getattr(record, 'reference', None)
                if path_vs_reference(path, reference):
                    found = record
                    break  # we found our record, no use to continue loop
            if not found:
                # path is of proper file type, but doesn't match any record
                orphans.append(path)
                continue
            # TODO path is record
            setattr(found, '_file_path', path)
            items[found.hid] = [path, ]

    # This code has one responsibility:
    # - populate empties, a dictionary of records H-ID with no file path
    empties = {}  # records with no file
    for hid, record in sorted(records.items()):
        if not getattr(record, '_file_path', None):
            empties[hid] = record

    from .signals import Stopper
    from time import sleep
    stopper = Stopper()

    # This code has two responsibilities:
    # - create one Nakala item for each key of dictionnary items
    # - upload a file in this item for each file path of key's value
    for hid, paths in sorted(items.items()):
        record = records[hid]
        for path in paths:
            print('TODO: upload "%s" %s' % (path, record))
            if stopper.stopped:
                break
            sleep(0.01)

        sleep(0.01)

    print()
    if stopper.stopped:
        print("Stopped !")
    print("Done. %s items, %s orphan, %s empty."
          % (len(items), len(orphans), len(empties)))
    exit()

    fd = open(args.output, 'w', newline='')
    report = csv.writer(fd, delimiter=',')
    report.writerow(['key', 'handle', 'hash', 'web', 'raw'])  # col names

    rows = {}
    with open(args.input, newline='') as f:
        reader = csv.reader(f, delimiter=args.separator, quotechar='"')
        # read first row
        columns = get_columns(reader, [
            args.key, args.created, args.issued, args.path,
            args.title, args.description, args.creators,
            ])
        # read last rows (ie. every row excluding the first)
        done = {}
        for row in progress(list(reader)):
            key = int(row[columns[args.key]])  # TODO Flora-specific
            img = row[columns[args.path]]
            paths = get_local_image_paths(args.files_dir, img)
            if not paths:  # TODO should I upload a data with no file?
                continue

            files = []  # files to upload (= paths minus crap)
            for path in paths:
                path = find_file(key, path)
                if not path:
                    continue  # we cannot upload this

                other = is_already_uploaded(key, path, done, rows)
                if other:
                    rows[key] = rows[other]
                    write_csv(report, rows[key], REPO_URL, API_URL)
                    continue  # don't reupload files referenced multiple times
                files.append(path)
            if not files:
                logging.info("⚠️ ;CREATE;Collection;%9s;%s;No File", key, data)
                continue

            creators = get_authors(row[columns[args.creators]], authors)
            data = create_data(row, columns, args, creators, SETTINGS, files)
            API_CALL(data, 'create', args, key)
            for index, path in enumerate(data.paths):
                done[path] = key
                payload = {
                    'key': key,
                    'handle': data.id,
                    'hash': data.get_hash(index),
                    }
                write_csv(report, payload, REPO_URL, API_URL)
                lst = rows.get(key, [])
                lst.append(payload)
                rows[key] = lst

            if args.cleanup:
                API_CALL(data, 'delete', args, key)

            sleep(0.1)

    if args.cleanup:
        API_CALL(collection, 'delete', args)

    check_orphaned_files(args.files_dir, done.keys())

    fd.close()
