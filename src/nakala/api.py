# -*- coding: utf-8 -*-
from .settings import API_URL, API_KEY
import requests
from requests.exceptions import HTTPError
from urllib.parse import quote
from json import dumps
from os.path import join


class NakalaObject:
    def __init__(self, public, name, language):
        self.id = None
        self._public = public
        self.name = name
        self.language = language
        self.baseurl = API_URL
        self._headers = {
                'X-API-KEY': API_KEY,
                'Content-Type': 'application/json'
                }

    @property
    def url(self):
        try:
            return join(self.baseurl, quote(self.id, safe=''))
        except TypeError:  # self.id is None
            return self.baseurl

    def raiseError(self, response):
        from json import loads
        start = response.text.index('{')
        end = response.text.index('}')+1
        error_as_json = response.text[start:end]
        message = loads(error_as_json)['message']
        trace = (response.status_code, self.baseurl, message)
        raise HTTPError('[%s] %s: %s' % trace)

    def __repr__(self):
        return '%s;?;;"%s"' % (self.id if self.id else '~', self.name)


class Collection(NakalaObject):
    def __init__(self, public, name, rights=None, language='fr'):
        super(Collection, self).__init__(public, name, language)
        self.baseurl = join(API_URL, 'collections')
        self.rights = rights or []
        self._post_data = {
            'status': self.public,
            'rights': self.rights,
            'metas': [
                {
                    'value': self.name,
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#string',
                    'propertyUri': 'http://nakala.fr/terms#title'
                }
            ]
        }

    @property
    def public(self):
        return 'public' if self._public else 'private'

    def retrieve(self):
        if self.id:
            response = requests.get(
                    self.url,
                    headers=self._headers,
                    )
            if response.status_code != requests.codes.ok:
                self.id = None
                self.raiseError(response)
        return self.id

    def create(self):
        if not self.id:
            response = requests.post(
                    self.baseurl,
                    headers=self._headers,
                    data=dumps(self._post_data),
                    )
            if response.status_code != requests.codes.created:
                self.raiseError(response)
            self.id = response.json()['payload']['id']
        return self.id

    def delete(self):
        if not self.id:
            return True
        response = requests.delete(self.url, headers=self._headers)
        if response.status_code == requests.codes.no_content:
            self.id = None
        return self.id is None


def now():
    from time import gmtime, strftime
    # Nakala only accepts year-month or year-month-day
    return strftime("%Y-%m-%d", gmtime())


class Data(NakalaObject):
    def __init__(
            self, public, license,
            collection, name, description,
            created=None, issued=None,
            creators=None, contributor=None, publisher=None,
            paths=None, rights=None, language='fr'):
        super(Data, self).__init__(public, name, language)
        self.baseurl = join(API_URL, 'datas')
        self.collection = collection
        self.description = description
        self.contributor = None
        if contributor:
            orcid = contributor.get('orcid', None)
            self.contributor = '%s %s%s' % (
                    contributor['surname'].upper(),
                    contributor['givenname'],
                    (', orcid: %s' % orcid) if orcid else ''
                    )
        self.creators = creators or []
        self.created = created or None
        self.issued = issued or now()
        self.publisher = publisher
        self.license = license
        self.rights = rights or []
        self.paths = paths or []
        if not self.paths:
            raise ValueError('422: At least one file must be added to data.')
        self._post_data = {
            'status': self.published,
            'collectionsIds': [self.collection, ],
            'files': [],
            'rights': self.rights,
            'metas': [
                {
                    # visual representation other than text  # TODO hard-coded
                    'value': 'http://purl.org/coar/resource_type/c_c513',
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#anyURI',
                    'propertyUri': 'http://nakala.fr/terms#type',
                },
                {
                    'value': self.name,
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#string',
                    'propertyUri': 'http://nakala.fr/terms#title',
                },
                {
                    'value': self.description,
                    'lang': self.language,
                    'typeUri': 'http://purl.org/dc/terms/DCMIType',
                    'propertyUri': 'http://purl.org/dc/terms/description',
                },
                {
                    'value': self.contributor,
                    'lang': self.language,
                    'typeUri': 'http://purl.org/dc/terms/DCMIType',
                    'propertyUri': 'http://purl.org/dc/terms/contributor',
                },
                {
                    'value': self.license,
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#string',
                    'propertyUri': 'http://nakala.fr/terms#license',
                },
            ]
        }
        if self.publisher:
            self._post_data['metas'].append({
                    'value': self.publisher,
                    'lang': self.language,
                    'typeUri': 'http://purl.org/dc/terms/DCMIType',
                    'propertyUri': 'http://purl.org/dc/terms/publisher',
                })
        for creator in self.creators:
            self._post_data['metas'].append({
                    'value': creator,
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#string',
                    'propertyUri': 'http://nakala.fr/terms#creator',
                })
        if self.created:
            self._post_data['metas'].append({
                    'value': self.created,
                    'lang': self.language,
                    'typeUri': 'http://www.w3.org/2001/XMLSchema#string',
                    'propertyUri': 'http://nakala.fr/terms#created',
                })
        if self.issued:
            self._post_data['metas'].append({
                    'value': self.issued,
                    'lang': self.language,
                    'typeUri': 'http://purl.org/dc/terms/DCMIType',
                    'propertyUri': 'http://purl.org/dc/terms/issued',
                })

    @property
    def published(self):
        return 'published' if self._public else 'pending'

    def get_mime(self, index=0):
        if not hasattr(self, '_mimetypes'):
            from mimetypes import MimeTypes
            mime = []
            for path in self.paths:
                mime.append(MimeTypes().guess_type(path))
            setattr(self, '_mimetypes', mime)
        return self._mimetypes[index]

    def get_hash(self, index=0):
        if not hasattr(self, '_hashes'):
            import hashlib
            hashes = []
            for path in self.paths:
                f = open(path, 'rb')
                hash_ = hashlib.sha1(f.read()).hexdigest()
                f.close()
                hashes.append(hash_)
            setattr(self, '_hashes', hashes)
        return self._hashes[index]

    def create(self, upload=True):
        if upload:
            try:
                self.upload()
            except HTTPError as ex:
                print(ex)
        if not self.id:
            response = requests.post(
                    self.baseurl,
                    headers=self._headers,
                    data=dumps(self._post_data),
                    )
            if response.status_code != requests.codes.created:
                self.raiseError(response)
            self.id = response.json()['payload']['id']
        return self.id

    def upload(self):
        url = join(self.baseurl, 'uploads')
        headers = self._headers.copy()
        headers['accept'] = headers.pop('Content-Type')
        for index, path in enumerate(self.paths):
            with open(path, 'rb') as f:
                files = {'file': (path, f, self.get_mime(index))}
                answer = requests.post(url, files=files, headers=headers)
                response = answer.json()

            if response.get('code', 201) != requests.codes.created:
                trace = (self.baseurl, response['code'], response['message'])
                raise HTTPError('%s: [%s] %s' % trace)

            hash_ = self.get_hash(index)
            if response['sha1'] != hash_:
                message = 'local: %s, remote: %s' % (hash_, response['sha1'])
                trace = (self.baseurl, requests.codes.created, message)
                raise HTTPError('%s: [%s] hashes are inconsistent: %s' % trace)

            self._post_data['files'].append({'sha1': hash_})

    def delete(self):
        if not self.id:
            return True
        response = requests.delete(self.url, headers=self._headers)
        if response.status_code != requests.codes.no_content:
            response = response.json()
            trace = (self.baseurl, response['code'], response['message'])
            raise HTTPError('%s: [%s] %s' % trace)
        self.id = None
        return True

    def __repr__(self):
        id_ = self.id if self.id else '~'
        creators = []
        if self.contributor:
            creators.append(self.contributor)
        creators = '|'.join(creators) if creators else '?'
        paths = '|'.join(self.paths) if self.paths else '-'
        return '%s;%s;%s;%s' % (id_, creators, self.created, paths)
