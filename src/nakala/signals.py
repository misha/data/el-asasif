from signal import signal, SIGINT, SIGTERM
from time import sleep


class Stopper(object):
    def __init__(self):
        signal(SIGINT, self.stop)
        signal(SIGTERM, self.stop)
        self.stopped = False

    def stop(self, *args):
        self.stopped = True


if __name__ == '__main__':
    stopper = Stopper()
    print("Program started ; press CTRL+C to stop it.")
    while not stopper.stopped:
        sleep(0.1)
        print(".", end='', flush=True)

    print("\nProgram stopped.")
