# -*- coding: utf-8 -*-


REPO_URL = 'https://test.nakala.fr/'
API_URL = 'https://apitest.nakala.fr/'
API_KEY = 'f41f5957-d396-3bb9-ce35-a4692773f636'
COLLECTIONS = [
    {
      'name': "El-Asasif",
      'public': False,
      'rights': [
        # by default:
        # { 'role': 'ROLE_OWNER', 'id': API_KEY, },  # by default
        # test users:
        {'role': 'ROLE_EDITOR', 'id': 'f4f565c4-646b-11eb-817d-5254008c9c26'},
        ],
      'publisher': "Université de Strasbourg",
      # curl https://apitest.nakala.fr/vocabularies/licenses
      'license': 'CC-BY-NC-ND-4.0',
    },
  ]
