# -*- coding: utf-8 -*-
from __future__ import unicode_literals  # we want unicode strings in py2.x too
from sys import version_info
from unittest import TestCase, skipUnless
from io import StringIO
from csv_util import Reader


class ReadCSVTest(TestCase):

    def setUp(self):
        input_content = '''"one","two","two and a half"
1,two,"2,5"
2,four,five'''
        self.input_file = StringIO(input_content)

    def test_read_header(self):
        reader = Reader()
        reader.read(self.input_file)
        self.assertEqual(3, len(reader.header))
        self.assertEqual('one', reader.header[0])
        self.assertEqual('two', reader.header[1])
        self.assertEqual('two and a half', reader.header[2])

    def test_read_rows_count(self):
        reader = Reader()
        reader.read(self.input_file)
        self.assertEqual(len(reader.rows), 2)

    def test_read_cells_contents(self):
        reader = Reader()
        reader.read(self.input_file)
        self.assertEqual(reader.get_cell(0, 'one'), '1')
        self.assertEqual(reader.get_cell(0, 'two'), 'two')
        self.assertEqual(reader.get_cell(0, 'two and a half'), '2,5')
        self.assertEqual(reader.get_cell(1, 'one'), '2')
        self.assertEqual(reader.get_cell(1, 'two'), 'four')
        self.assertEqual(reader.get_cell(1, 'two and a half'), 'five')

    def test_reader_has_rows_but_no_callback(self):
        reader = Reader()
        self.assertTrue(hasattr(reader, 'rows'))
        self.assertFalse(hasattr(reader, 'callback'))

    def test_reader_has_callback_but_now_rows(self):
        def on_row(header, row):
            pass  # pragma: no cover
        reader = Reader(callback=on_row)
        self.assertFalse(hasattr(reader, 'rows'))
        self.assertTrue(hasattr(reader, 'callback'))

    def test_read_with_custom_callback(self):

        class Dummy(object):
            def __init__(self, uid, data):
                self.uid = uid
                self.data = data

        objects = []

        def create_object(header, row):
            uid = Reader.get('one', header, row)
            objects.append(Dummy(uid, row))

        reader = Reader(callback=create_object)
        reader.read(self.input_file)

        self.assertEqual(len(objects), 2)
        self.assertEqual(objects[0].uid, '1')
        self.assertEqual(objects[1].uid, '2')

    def test_read_error_with_int_callback(self):
        reader = Reader(callback=42)
        with self.assertRaises(TypeError) as context:
            reader.read(self.input_file)
        self.assertTrue('int' in str(context.exception))

    def test_read_error_with_single_param_callback(self):
        def wrong_profile(single_param):
            pass  # pragma: no cover
        reader = Reader(callback=wrong_profile)
        with self.assertRaises(TypeError) as context:
            reader.read(self.input_file)
        self.assertTrue('wrong_profile' in str(context.exception))
        self.assertTrue('1' in str(context.exception))

    @skipUnless(version_info < (3, 0), 'Python2 rants about number of params')
    def test_read_error_with_more_than_two_params_callback_py2x(self):
        def wrong_profile(huey, dewey, louie):
            pass  # pragma: no cover
        reader = Reader(callback=wrong_profile)
        with self.assertRaises(TypeError) as context:
            reader.read(self.input_file)
        self.assertTrue('wrong_profile' in str(context.exception))
        # error message is slightly different in Python 2.7 than 3.x
        self.assertTrue('3' in str(context.exception))

    @skipUnless(version_info >= (3, 0), 'Python3 names all missing parameters')
    def test_read_error_with_more_than_two_params_callback_py3x(self):
        def wrong_profile(huey, dewey, louie):
            pass  # pragma: no cover
        reader = Reader(callback=wrong_profile)
        with self.assertRaises(TypeError) as context:
            reader.read(self.input_file)
        self.assertTrue('wrong_profile' in str(context.exception))
        # error message is slightly different in Python 2.7 than 3.x
        self.assertTrue('louie' in str(context.exception))
