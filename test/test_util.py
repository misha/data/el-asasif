# -*- coding: utf-8 -*-
from unittest import TestCase
from parameterized import parameterized
from util import get_prefix, path_vs_reference


class MyTest(TestCase):

    @parameterized.expand([
        ('CROQ_00109', 'CROQ_'),
        ('AS_2022_00042', 'AS_2022_'),
        ('WTF_01_WAT_TOTO', 'WTF_01_WAT_'),
        ])
    def test_get_prefix(self, value, expected):
        self.assertEqual(expected, get_prefix(value))

    @parameterized.expand([
        ('/dir/subdir/PREFIX_00042.ext', 'PREFIX_042', True),
        ('/dir/subdir/AS_2022', 'AS_2022', True),
        ('WHATEVER_RLY_42', 'WHATEVER_RLY_42', True),
        ('NOT_INT', 'NOT_INT', True),
        ('NOT_INT', 'SUMTHIN_ELSE', False),
        ('AS_2022', 'SUMTHIN_ELSE', False),
        ('SUMTHIN', 'AS_2022', False),
        ])
    def test_path_vs_reference(self, path, reference, expected):
        self.assertEqual(expected, path_vs_reference(path, reference))
